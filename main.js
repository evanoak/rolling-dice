var count = [0,0,0,0,0,0,0,0,0,0,0,0,0];
let possibleSums = [2,3,4,5,6,7,8,9,10,11,12];
function rollDice(){
    return Math.ceil(Math.random()*6) + Math.ceil(Math.random()*6);
}
for(let i =1; i<=1000;i++){
    let result = rollDice()
    count[result] = count[result] + 1;
} 
let destination = document.getElementById('container');
let notAGraph = document.getElementById('numberOutput');
for (let k = 2; k <= 12; k++){
    let graphRow = document.createElement('div');
    graphRow.style.display = 'flex'
    let header = document.createElement('div')
    header.style.width = '30px'
    let headerText = document.createTextNode(possibleSums[k - 2])
    header.appendChild(headerText)
    graphRow.appendChild(header)
    let bar = document.createElement('div')
    bar.style.backgroundColor = 'MediumSpringGreen'
    bar.style.width = count[k] +'px'
    bar.style.borderColor = 'black'
    bar.style.border = 'black 1px solid'
    let divText = document.createTextNode(count[k])
    bar.appendChild(divText)
    graphRow.appendChild(bar)
    destination.appendChild(graphRow)
} 
for (let l = 2; l <= 12; l++){
    let graphRow = document.createElement('div');
    graphRow.style.display = 'flex'
    let header = document.createElement('div')
    header.style.width = '29px'
    let headerText = document.createTextNode(possibleSums[l - 2] + ':')
    header.appendChild(headerText)
    graphRow.appendChild(header)
    let divText = document.createTextNode(count[l])
    graphRow.appendChild(divText)
    notAGraph.appendChild(graphRow)
}